
#define HSE_VALUE ((uint32_t)8000000) /* STM32 discovery uses a 8Mhz external crystal */

#include "stm32f4xx_conf.h"
#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_adc.h"
#include "usbd_cdc_core.h"
#include "usbd_usr.h"
#include "usbd_desc.h"
#include "usbd_cdc_vcp.h"
#include "usb_dcd_int.h"

//for fpu math operations
#include "math.h"


#include "own_libs/defines.h"
#include "adc_helper.h"
#include "usb_helper.h"
#include "imu_helper.h"
#include "timer_helper.h"
#include "data_interface.h"
#include "gpio_helper.h"

#include "tm_stm32f4_i2c.h"




volatile uint32_t ticker, downTicker;


void init();

/*
 * Define prototypes for interrupt handlers here. The conditional "extern"
 * ensures the weak declarations from startup_stm32f4xx.c are overridden.
 */
#ifdef __cplusplus
 extern "C" {
#endif

void SysTick_Handler(void);
void NMI_Handler(void);
void HardFault_Handler(void);
void MemManage_Handler(void);
void BusFault_Handler(void);
void UsageFault_Handler(void);
void SVC_Handler(void);
void DebugMon_Handler(void);
void PendSV_Handler(void);



#ifdef __cplusplus
}
#endif


 uint16_t mcu_1_pwm = 4000;
 uint16_t mcu_2_pwm = 4000;

 //Initialasing the struct for a IMU
 IMU_struct imu_1_settings = { 0x3A, 0xD6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.0074768,
 					0.0000610351, 0.0000610351, I2C1, 100000, TM_I2C_PinsPack_2 ,0};

 IMU_struct imu_2_settings = { 0x3A, 0xD6, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.0074768,
 					0.0000610351, 0.0000610351, I2C2, 100000, TM_I2C_PinsPack_1 ,0};

 IMU_struct imu_3_settings = { 0x3A, 0xD6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.0074768,
 					0.0000610351, 0.0000610351, I2C3, 100000, TM_I2C_PinsPack_1 ,0};


 //dummy data struct for testing
 Slave_Data slave_data = {
		 SLAVE_ID,
		 2,
		 3,
		 4,
		 5,
		 6,
		 7,
		 8,
		 9,
		 10,
		 11,
		 12,
		 13,
		 14,
		 15,
		 16,
		 17,
		 18,
		 19,
		 20,
		 21,
		 22,
		 23,
		 24,
		 25,
		 26,
		 27,
		 28,
		 29,
		 30,
		 31,
		 32,
		 33,
		 34,
		 35,
		 36,
		 37,
		 38,
		 39,
		 40,
		 41
 };

 Master_Data master_data = {
		199,
		{13,9,2,1,88},
		{0,0,0,0,0},
		0,
		5,
		0,
		6,
		0,
		7,
		8
	};



int main(void)
{
	/* Set up the system clocks */
	SystemInit();

	wait_cycles(10000);

	/* Initialize USB, IO, SysTick, and all those other things you do in the morning */
	init();

	wait_cycles(10000);




	//////////////////////////////////////////////
	//	Current USB hack to get working offset	//
	//////////////////////////////////////////////
#if USB_COMM_ON
	int usb_recv_counter = 0;

	while(usb_recv_counter < 25)
	{
		uint8_t theByte;
		if (VCP_get_char(&theByte))
		{
			usb_recv_counter++;
			if(theByte == 'Z')
				break;
		}

	}


	usb_recv_counter = 0;
#endif
	//////////////////////////////////////////////
	//				End of USB Hack				//
	//////////////////////////////////////////////

	//wait 100ms to give MCU time before setting enable to high
	wait_cycles(16800000);


	//enable mcu pin if mcu is on
	#if MCU1_ON
		set_pin_high(GPIO_Pin_0);
	#endif

	#if MCU2_ON
		set_pin_high(GPIO_Pin_1);
	#endif



	while (1)
	{
#if USB_COMM_ON
		uint8_t* master_data_pointer = &master_data;

		while(usb_recv_counter < sizeof(Master_Data))
		{
			if (VCP_get_char(master_data_pointer))
			{
				master_data_pointer++;
				usb_recv_counter++;
			}
		}

		usb_recv_counter = 0;
#endif


		//////////////////////////////////////////////
		//			Read all sensor data			//
		//////////////////////////////////////////////
/*
		#if SOFT_POT1_ON
			slave_data.SOFT_POT_1 = ADCH_getValue(ADC2, ADC_Channel_0);
		#endif

		#if SOFT_POT2_ON
			slave_data.SOFT_POT_2 = ADCH_getValue(ADC2, ADC_Channel_2);
		#endif

		#if SOFT_POT3_ON
			slave_data.SOFT_POT_3 = ADCH_getValue(ADC2, ADC_Channel_4);
		#endif

		#if HIP_POT1_ON
			slave_data.HIP_POT_1 = ADCH_getValue(ADC2, ADC_Channel_6);
		#endif

		#if HIP_POT2_ON
			slave_data.HIP_POT_2 = ADCH_getValue(ADC2, ADC_Channel_14);
		#endif

*/
		#if IMU1_ON
			update_gyro_acc(&imu_1_settings);
			fill_imu_data(&slave_data, &imu_1_settings);
		#endif

		#if IMU2_ON
			update_gyro_acc(&imu_2_settings);
			fill_imu_data(&slave_data, &imu_2_settings);
		#endif

		#if IMU3_ON
			update_gyro_acc(&imu_3_settings);
			fill_imu_data(&slave_data, &imu_3_settings);
		#endif

		wait_cycles(100000);




		//////////////////////////////////////////////
		//		Process sensor data and send		//
		//////////////////////////////////////////////

#if USB_COMM_ON
		/*if(slave_data.ID < 255)
		{
			slave_data.ID++;
		}else{
			slave_data.ID = 0;
		}*/
		uint8_t* pointer2 = &slave_data;
		VCP_send_buffer(pointer2,sizeof(Slave_Data));
#endif

		wait_cycles(10000);


	}

	return 0;
}


void init()
{
#if USB_COMM_ON
	init_usb();
#endif

	//ADCH_init();

	#if SOFT_POT1_ON
		ADCH_init(ADC2, GPIOA, GPIO_Pin_0, ADC_Channel_0);
	#endif

	#if SOFT_POT2_ON
		ADCH_init(ADC1, GPIOA, GPIO_Pin_5, ADC_Channel_5);
	#endif

	#if SOFT_POT3_ON
		ADCH_init(ADC2, GPIOA, GPIO_Pin_4, ADC_Channel_4);
	#endif

	#if HIP_POT1_ON
		ADCH_init(ADC2, GPIOA, GPIO_Pin_0, ADC_Channel_0);
	#endif

	#if HIP_POT2_ON
		ADCH_init(ADC2, GPIOA, GPIO_Pin_4, ADC_Channel_4);
	#endif

	#if IMU1_ON
		init_imu(imu_1_settings);
	#endif

	#if IMU2_ON
		init_imu(imu_2_settings);
	#endif

	#if IMU3_ON
		init_imu(imu_3_settings);
	#endif

	//init and set mcu enable pin to low
	init_out_pin(GPIO_Pin_0);
	init_out_pin(GPIO_Pin_1);


	init_sys_timer();
	init_pwm();

	return;
}


/*======================================
 *==========Interrupt Handlers==========
 *======================================
 */

//timer will be executed all 10 ms according to timer_helper.h
void SysTick_Handler(void)
{
	#if POS_CONTROLL_ON
		#if SOFT_POT1_ON
			slave_data.SOFT_POT_1 = ADCH_getValue(ADC2, ADC_Channel_0);
		#endif

		#if SOFT_POT2_ON
			slave_data.SOFT_POT_2 = ADCH_getValue(ADC1, ADC_Channel_5);
		#endif

		#if SOFT_POT3_ON
			slave_data.SOFT_POT_3 = ADCH_getValue(ADC2, ADC_Channel_4);
		#endif

		#if HIP_POT1_ON
			slave_data.HIP_POT_1 = ADCH_getValue(ADC2, ADC_Channel_0);
		#endif

		#if HIP_POT2_ON
			slave_data.HIP_POT_2 = ADCH_getValue(ADC2, ADC_Channel_4);
		#endif
		//run position control
		mcu_1_pwm = position_controll_mcu(&master_data, &slave_data, 1);
		set_mcu1_pwm(mcu_1_pwm);
		mcu_2_pwm = position_controll_mcu(&master_data, &slave_data, 2);
		set_mcu2_pwm(mcu_2_pwm);
	#endif

}



void NMI_Handler(void)       {}
void HardFault_Handler(void) {}
void MemManage_Handler(void) {}
void BusFault_Handler(void)  {}
void UsageFault_Handler(void){}
void SVC_Handler(void)       {}
void DebugMon_Handler(void)  {}
void PendSV_Handler(void)    {}
