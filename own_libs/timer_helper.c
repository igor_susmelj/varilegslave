#include "stm32f4xx_rcc.h"
#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_tim.h"

#include "timer_helper.h"
#include "data_interface.h"

#define MCU_PWM_PRESCALER 15


//initializes system timer (used for the position control)
void init_sys_timer()
{
	SysTick_Config(SystemCoreClock/ TIMER_HELPER_FREQUENCY);
}


//initializes pwm on pin PC6 (CH1) and PC8 (CH3)
void init_pwm()
{

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
    GPIO_InitTypeDef gpioStructure;
    gpioStructure.GPIO_Pin = GPIO_Pin_6;
    gpioStructure.GPIO_Mode = GPIO_Mode_AF;
    gpioStructure.GPIO_Speed = GPIO_Speed_50MHz;
    gpioStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOC, &gpioStructure);

    gpioStructure.GPIO_Pin = GPIO_Pin_8;
    GPIO_Init(GPIOC, &gpioStructure);

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
    TIM_TimeBaseInitTypeDef timerInitStructure;
    timerInitStructure.TIM_Prescaler = MCU_PWM_PRESCALER;
    timerInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    timerInitStructure.TIM_Period = MCU_PWM_RESOLUTION;
    timerInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    timerInitStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM3, &timerInitStructure);
    TIM_Cmd(TIM3, ENABLE);

    TIM_OCInitTypeDef outputChannelInit = {0,};
    outputChannelInit.TIM_OCMode = TIM_OCMode_PWM1;
    outputChannelInit.TIM_Pulse = 4096;
    outputChannelInit.TIM_OutputState = TIM_OutputState_Enable;
    outputChannelInit.TIM_OCPolarity = TIM_OCPolarity_High;

    TIM_OC3Init(TIM3, &outputChannelInit);
    TIM_OC3PreloadConfig(TIM3, TIM_OCPreload_Enable);

    TIM_OC1Init(TIM3, &outputChannelInit);
    TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);


    GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_TIM3);
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource8, GPIO_AF_TIM3);
}

//set pwm value from mcu 1 to pwm value
inline void set_mcu1_pwm(uint16_t pwm)
{
	TIM_SetCompare1(TIM3,pwm);
}

//set pwm value from mcu 2 to pwm value
inline void set_mcu2_pwm(uint16_t pwm)
{
	TIM_SetCompare3(TIM3,pwm);
}

//wait the given amount of CPU cylces
//used to simulate high CPU load
inline void wait_cycles(unsigned int cycles)
{
	unsigned int i = 0;
	while(i < cycles)
		++i;
}


