#ifndef __TIMER_HELPER_H__
#define __TIMER_HELPER_H__



#define TIMER_HELPER_FREQUENCY 200	//set timer to 100Hz -> 10ms

//PWM TIMER
#define PWM_TIMER_PRESCALER 20

//initializes sys timer
void init_sys_timer();

//sets PWM with TIM4
void init_pwm();

//waits given amount of CPU cycles
inline void wait_cycles(unsigned int cycles);

#endif
