#ifndef __DATA_INTERFACE_H__
#define __DATA_INTERFACE_H__

#include "own_libs/defines.h"


//Data from Master to Slave
typedef struct {

	//Counter - for identifying packets
	uint8_t COUNTER;

	//MCU 1 PWM Data
	uint16_t MCU1_POS[POS_CONTROLL_VAL_COUNT];

	//MCU 2 PWM Data
	uint16_t MCU2_POS[POS_CONTROLL_VAL_COUNT];

	uint32_t FLAGS;


	//MCU 1 PID Position Controll values
	float MCU1_PID_P;
	float MCU1_PID_I;
	float MCU1_PID_D;

	//MCU 2 PID Position Controll values
	float MCU2_PID_P;
	float MCU2_PID_I;
	float MCU2_PID_D;

}Master_Data;



typedef struct {
	//slave ID		0->left macceppa	1->back		2->right macceppa
	uint8_t ID;

	//computation time of slave
	uint8_t TIME;

	//data of IMU 1
	int16_t IMU1_AX;
	int16_t IMU1_AY;
	int16_t IMU1_AZ;

	int16_t IMU1_GX;
	int16_t IMU1_GY;
	int16_t IMU1_GZ;

	//data of IMU 2
	int16_t IMU2_AX;
	int16_t IMU2_AY;
	int16_t IMU2_AZ;

	int16_t IMU2_GX;
	int16_t IMU2_GY;
	int16_t IMU2_GZ;

	//data of IMU 3
	int16_t IMU3_AX;
	int16_t IMU3_AY;
	int16_t IMU3_AZ;

	int16_t IMU3_GX;
	int16_t IMU3_GY;
	int16_t IMU3_GZ;


	//data of soft-pots
	uint16_t SOFT_POT_1;	//knee ankle
	uint16_t SOFT_POT_2;	//lever arm
	uint16_t SOFT_POT_3;	//roll


	//data of load cells
	uint8_t LOAD_CELL_1;
	uint8_t LOAD_CELL_2;
	uint8_t LOAD_CELL_3;
	uint8_t LOAD_CELL_4;
	//uint8_t LOAD_CELL_5;
	//uint8_t LOAD_CELL_6;


	//data of MCU 1
	int8_t MCU1_CURRENT;
    int16_t MCU1_RPM;
	int16_t MCU1_TICK_OFFSET;
	uint8_t MCU1_TEMP;

	//data of MCU 2
	int8_t MCU2_CURRENT;
    int16_t MCU2_RPM;
	int16_t MCU2_TICK_OFFSET;
	uint8_t MCU2_TEMP;


	//data of temp sensors
	uint8_t TEMP_1;
	uint8_t TEMP_2;


	//========OTHER DATA========//

	uint16_t USER_INPUT;

	//Backup potentiometer hip
	uint16_t HIP_POT_1;		//left hip pot
	uint16_t HIP_POT_2;		//right hip pot

	//Voltages
	uint8_t VOLTAGE_1;
	uint8_t VOLTAGE_2;

}Slave_Data;











#endif
