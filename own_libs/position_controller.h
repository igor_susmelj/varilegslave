#ifndef __POSITION_CONTROLLER_H__
#define __POSITION_CONTROLLER_H__

#include "data_interface.h"

/*
 * Assumption:
 * MCU: -4000 rpm -> 0V
 * 		4000  rpm -> 3V
 *
 * transfer function:	-418 + (ADC_VALUE / 4096) * 836
 */

// Transfer functions

#define MCU_ADC_TO_RAD(x) (-418 + ((x)/4096.)*836.)
#define SOFT_POT_TO_RAD(x) (((x - 1175.)/4096.)*0.9*3.142)
#define LeftHipPot2Rad(x) ((3.142/3.)/(2632.-915.)*(x-915.))
#define LeftWinchPot2Rad(x) (0.019/(2979.-2398.)*(x-2398.)/0.03)
#define LeftLeverPot2Rad(x) (3.142/2./(2826.-1666.)*(x-1666.))
#define LeftKneePot2Rad(x) (3.142/2./(2599.-1638.)*(x-1638.))

#define RightHipPot2Rad(x) ((3.142/3.)/(2626.-941.)*(x-941.))
#define RightWinchPot2Rad(x) (0.019/(2520-1970)*(x-1970)/0.03)
#define RightLeverPot2Rad(x) (3.142/2./(2933.-1755.)*(x-1755.))
#define RightKneePot2Rad(x) (3.142/2./(2638.-1672.)*(x-1672.))

// Settings

#define SAFE_STATE 0
#define CHECK_MCU_ENABLES 1

#define CONTROL_FILTER 1

#define TESTING_ARRAY 0
#define PACKAGE_CHECK 0
#define USE_KNEE_FOR_LEVER 0

#define SAFETY_ON 1
// define which safety measures are used
#define SAFETY_INPUT_VALUES 1 // check if send motor values do not violate joint limits --> USB Communication problem --> rpm = 0
#define SAFETY_SENSOR_VALUES 0 // check if sensor values make sense: do not jump by more than XXX --> unplugged sensor --> rpm = 0  || not jet implemented
#define SAFETY_VIRTUAL_STOPPERS_1 1 // check if sensor values indicate motor is in slightly critical region --> rpm slow away from virtual stopper 1
#define SAFETY_VIRTUAL_STOPPERS_2 1 // check if sensor values indicate motor is in significantly critical region  --> rpm = 0 and enable safe state
#define SAFETY_KNEE_CHECK 1 // check if knee joint is overstretched

// Joint limits (raw values)

// back slave (id 2)

// left hip (mcu 1)
#define HIP_LEFT_MIN 10
#define HIP_LEFT_MIN_VIRTUAL_1 HIP_LEFT_MIN+300
#define HIP_LEFT_MIN_VIRTUAL_2 HIP_LEFT_MIN+150
#define HIP_LEFT_MAX 3588
#define HIP_LEFT_MAX_VIRTUAL_1 HIP_LEFT_MAX-400
#define HIP_LEFT_MAX_VIRTUAL_2 HIP_LEFT_MAX-200

// right hip (mcu 2)
#define HIP_RIGHT_MIN 10
#define HIP_RIGHT_MIN_VIRTUAL_1 HIP_RIGHT_MIN+300
#define HIP_RIGHT_MIN_VIRTUAL_2 HIP_RIGHT_MIN+150
#define HIP_RIGHT_MAX 3607
#define HIP_RIGHT_MAX_VIRTUAL_1 HIP_RIGHT_MAX-400
#define HIP_RIGHT_MAX_VIRTUAL_2 HIP_RIGHT_MAX-200

// left slave (id 1)
// left lever (mcu 1)
#define LEVER_LEFT_MIN 1147
#define LEVER_LEFT_MIN_VIRTUAL_1 LEVER_LEFT_MIN+225
#define LEVER_LEFT_MIN_VIRTUAL_2 LEVER_LEFT_MIN+125
#define LEVER_LEFT_MAX 2854
#define LEVER_LEFT_MAX_VIRTUAL_1 LEVER_LEFT_MAX-150
#define LEVER_LEFT_MAX_VIRTUAL_2 LEVER_LEFT_MAX-75

// left winch (mcu 2)
#define WINCH_LEFT_MIN 2189
#define WINCH_LEFT_MIN_VIRTUAL_1 WINCH_LEFT_MIN+100
#define WINCH_LEFT_MIN_VIRTUAL_2 WINCH_LEFT_MIN+50
#define WINCH_LEFT_MAX 3386
#define WINCH_LEFT_MAX_VIRTUAL_1 WINCH_LEFT_MAX-150
#define WINCH_LEFT_MAX_VIRTUAL_2 WINCH_LEFT_MAX-100

// left knee
#define KNEE_LEFT_MIN 1560
#define KNEE_LEFT_MIN_VIRTUAL_1 KNEE_LEFT_MIN+50
#define KNEE_LEFT_MIN_VIRTUAL_2 KNEE_LEFT_MIN+25
#define KNEE_LEFT_MAX 2666
#define KNEE_LEFT_MAX_VIRTUAL_1 KNEE_LEFT_MAX-50
#define KNEE_LEFT_MAX_VIRTUAL_2 KNEE_LEFT_MAX-25

// right slave (id 3)
// right lever (mcu 1)
#define LEVER_RIGHT_MIN 1199
#define LEVER_RIGHT_MIN_VIRTUAL_1 LEVER_RIGHT_MIN+225
#define LEVER_RIGHT_MIN_VIRTUAL_2 LEVER_RIGHT_MIN+125
#define LEVER_RIGHT_MAX 2976
#define LEVER_RIGHT_MAX_VIRTUAL_1 LEVER_RIGHT_MAX-150
#define LEVER_RIGHT_MAX_VIRTUAL_2 LEVER_RIGHT_MAX-75

// RIGHT winch (mcu 2)
#define WINCH_RIGHT_MIN 1574
#define WINCH_RIGHT_MIN_VIRTUAL_1 WINCH_RIGHT_MIN+100
#define WINCH_RIGHT_MIN_VIRTUAL_2 WINCH_RIGHT_MIN+50
#define WINCH_RIGHT_MAX 2791
#define WINCH_RIGHT_MAX_VIRTUAL_1 WINCH_RIGHT_MAX-110
#define WINCH_RIGHT_MAX_VIRTUAL_2 WINCH_RIGHT_MAX-90

// RIGHT knee
#define KNEE_RIGHT_MIN 1620
#define KNEE_RIGHT_MIN_VIRTUAL_1 KNEE_RIGHT_MIN+50
#define KNEE_RIGHT_MIN_VIRTUAL_2 KNEE_RIGHT_MIN+25
#define KNEE_RIGHT_MAX 2707
#define KNEE_RIGHT_MAX_VIRTUAL_1 KNEE_RIGHT_MAX-50
#define KNEE_RIGHT_MAX_VIRTUAL_2 KNEE_RIGHT_MAX-25

//global control variables

// K_marius must be = 0 if the second motor position is used for the motor enable

const float KP_11 = 2.5;
const float KI_11 = 0;
const float KD_11 = 0;
const float K_marius_11 = 0;

const float KP_12 = 2.;
const float KI_12 = 0.01;
const float KD_12 = 0;
const float K_marius_12 = 0;

const float KP_21 = 2.5;
const float KI_21 = 0;
const float KD_21 = 0;
const float K_marius_21 = 0;

const float KP_22 = 2.5;
const float KI_22 = 0;
const float KD_22 = 0;
const float K_marius_22 = 0;

const float KP_31 = 2.5;
const float KI_31 = 0;
const float KD_31 = 0;
const float K_marius_31 = 0;

const float KP_32 = 2;
const float KI_32 = 0.01;
const float KD_32 = 0;
const float K_marius_32 = 0;

// joint limits in rad

// back slave (id 2)

// left hip (mcu 1)
const float HIP_LEFT_MIN_RAD = LeftHipPot2Rad(HIP_LEFT_MIN);
const float HIP_LEFT_MIN_VIRTUAL_1_RAD = LeftHipPot2Rad(HIP_LEFT_MIN_VIRTUAL_1);
const float HIP_LEFT_MIN_VIRTUAL_2_RAD = LeftHipPot2Rad(HIP_LEFT_MIN_VIRTUAL_2);
const float HIP_LEFT_MAX_RAD = LeftHipPot2Rad(HIP_LEFT_MAX);
const float HIP_LEFT_MAX_VIRTUAL_1_RAD = LeftHipPot2Rad(HIP_LEFT_MAX_VIRTUAL_1);
const float HIP_LEFT_MAX_VIRTUAL_2_RAD = LeftHipPot2Rad(HIP_LEFT_MAX_VIRTUAL_2);

// right hip (mcu 2)
const float HIP_RIGHT_MIN_RAD = RightHipPot2Rad(HIP_RIGHT_MIN);
const float HIP_RIGHT_MIN_VIRTUAL_1_RAD = RightHipPot2Rad(HIP_RIGHT_MIN_VIRTUAL_1);
const float HIP_RIGHT_MIN_VIRTUAL_2_RAD = RightHipPot2Rad(HIP_RIGHT_MIN_VIRTUAL_2);
const float HIP_RIGHT_MAX_RAD = RightHipPot2Rad(HIP_RIGHT_MAX);
const float HIP_RIGHT_MAX_VIRTUAL_1_RAD = RightHipPot2Rad(HIP_RIGHT_MAX_VIRTUAL_1);
const float HIP_RIGHT_MAX_VIRTUAL_2_RAD = RightHipPot2Rad(HIP_RIGHT_MAX_VIRTUAL_2);

// left slave (id 1)
// left lever (mcu 1)
const float LEVER_LEFT_MIN_RAD = LeftLeverPot2Rad(LEVER_LEFT_MIN);
const float LEVER_LEFT_MIN_VIRTUAL_1_RAD = LeftLeverPot2Rad(LEVER_LEFT_MIN_VIRTUAL_1);
const float LEVER_LEFT_MIN_VIRTUAL_2_RAD = LeftLeverPot2Rad(LEVER_LEFT_MIN_VIRTUAL_2);
const float LEVER_LEFT_MAX_RAD = LeftLeverPot2Rad(LEVER_LEFT_MAX);
const float LEVER_LEFT_MAX_VIRTUAL_1_RAD = LeftLeverPot2Rad(LEVER_LEFT_MAX_VIRTUAL_1);
const float LEVER_LEFT_MAX_VIRTUAL_2_RAD = LeftLeverPot2Rad(LEVER_LEFT_MAX_VIRTUAL_2);

// left winch (mcu 2)
const float WINCH_LEFT_MIN_RAD = LeftWinchPot2Rad(WINCH_LEFT_MIN);
const float WINCH_LEFT_MIN_VIRTUAL_1_RAD = LeftWinchPot2Rad(WINCH_LEFT_MIN_VIRTUAL_1);
const float WINCH_LEFT_MIN_VIRTUAL_2_RAD = LeftWinchPot2Rad(WINCH_LEFT_MIN_VIRTUAL_2);
const float WINCH_LEFT_MAX_RAD = LeftWinchPot2Rad(WINCH_LEFT_MAX);
const float WINCH_LEFT_MAX_VIRTUAL_1_RAD = LeftWinchPot2Rad(WINCH_LEFT_MAX_VIRTUAL_1);
const float WINCH_LEFT_MAX_VIRTUAL_2_RAD = LeftWinchPot2Rad(WINCH_LEFT_MAX_VIRTUAL_2);

// left knee
const float KNEE_LEFT_MIN_RAD = LeftKneePot2Rad(KNEE_LEFT_MIN);
const float KNEE_LEFT_MIN_VIRTUAL_1_RAD = LeftKneePot2Rad(KNEE_LEFT_MIN_VIRTUAL_1);
const float KNEE_LEFT_MIN_VIRTUAL_2_RAD = LeftKneePot2Rad(KNEE_LEFT_MIN_VIRTUAL_2);
const float KNEE_LEFT_MAX_RAD = LeftKneePot2Rad(KNEE_LEFT_MAX);
const float KNEE_LEFT_MAX_VIRTUAL_1_RAD = LeftKneePot2Rad(KNEE_LEFT_MAX_VIRTUAL_1);
const float KNEE_LEFT_MAX_VIRTUAL_2_RAD = LeftKneePot2Rad(KNEE_LEFT_MAX_VIRTUAL_2);

// right slave (id 3)
// right lever (mcu 1)
const float LEVER_RIGHT_MIN_RAD = RightLeverPot2Rad(LEVER_RIGHT_MIN);
const float LEVER_RIGHT_MIN_VIRTUAL_1_RAD = RightLeverPot2Rad(LEVER_RIGHT_MIN_VIRTUAL_1);
const float LEVER_RIGHT_MIN_VIRTUAL_2_RAD = RightLeverPot2Rad(LEVER_RIGHT_MIN_VIRTUAL_2);
const float LEVER_RIGHT_MAX_RAD = RightLeverPot2Rad(LEVER_RIGHT_MAX);
const float LEVER_RIGHT_MAX_VIRTUAL_1_RAD = RightLeverPot2Rad(LEVER_RIGHT_MAX_VIRTUAL_1);
const float LEVER_RIGHT_MAX_VIRTUAL_2_RAD = RightLeverPot2Rad(LEVER_RIGHT_MAX_VIRTUAL_2);

// RIGHT winch (mcu 2)
const float WINCH_RIGHT_MIN_RAD = RightWinchPot2Rad(WINCH_RIGHT_MIN);
const float WINCH_RIGHT_MIN_VIRTUAL_1_RAD = RightWinchPot2Rad(WINCH_RIGHT_MIN_VIRTUAL_1);
const float WINCH_RIGHT_MIN_VIRTUAL_2_RAD = RightWinchPot2Rad(WINCH_RIGHT_MIN_VIRTUAL_2);
const float WINCH_RIGHT_MAX_RAD = RightWinchPot2Rad(WINCH_RIGHT_MAX);
const float WINCH_RIGHT_MAX_VIRTUAL_1_RAD = RightWinchPot2Rad(WINCH_RIGHT_MAX_VIRTUAL_1);
const float WINCH_RIGHT_MAX_VIRTUAL_2_RAD = RightWinchPot2Rad(WINCH_RIGHT_MAX_VIRTUAL_2);

// RIGHT knee
const float KNEE_RIGHT_MIN_RAD = RightKneePot2Rad(KNEE_RIGHT_MIN);
const float KNEE_RIGHT_MIN_VIRTUAL_1_RAD = RightKneePot2Rad(KNEE_RIGHT_MIN_VIRTUAL_1);
const float KNEE_RIGHT_MIN_VIRTUAL_2_RAD = RightKneePot2Rad(KNEE_RIGHT_MIN_VIRTUAL_2);
const float KNEE_RIGHT_MAX_RAD = RightKneePot2Rad(KNEE_RIGHT_MAX);
const float KNEE_RIGHT_MAX_VIRTUAL_1_RAD = RightKneePot2Rad(KNEE_RIGHT_MAX_VIRTUAL_1);
const float KNEE_RIGHT_MAX_VIRTUAL_2_RAD = RightKneePot2Rad(KNEE_RIGHT_MAX_VIRTUAL_2);


uint16_t position_controll_mcu(Master_Data *master_data, Slave_Data *slave_data, int mcu_origin);
uint16_t PID_control_MCU(float KP, float KI, float KD, float K_marius, float* previous_error, int* motor_position_int_old, const uint16_t MCU_POS[POS_CONTROLL_VAL_COUNT], float joint_angle, const uint16_t joint_angle_raw, const int8_t motor_direction, const  uint8_t iterator);
uint16_t safety_check(Slave_Data *slave_data, uint16_t new_pwm_speed, const uint16_t MCU_POS[POS_CONTROLL_VAL_COUNT], const  uint8_t iterator,float joint_angle, const uint16_t joint_angle_raw, const uint16_t min_virtual_1, const uint16_t min_virtual_2, const uint16_t max_virtual_1, const uint16_t max_virtual_2, const float min_virtual_1_rad, const float min_virtual_2_rad, const float max_virtual_1_rad, const float max_virtual_2_rad);
void call_safe_state(Slave_Data *slave_data);
uint16_t filtering(uint16_t new_data,uint16_t sensor);
void disable_MCU(const uint16_t MCU1_POS1,const uint16_t MCU2_POS1);
void init_IMU(Slave_Data *slave_data);

#endif
