#ifndef __GPIO_HELPER_H__
#define __GPIO_HELPER_H__

void init_out_pin(uint32_t pin);
void init_in_pin(uint32_t pin);

inline void set_pin_high(uint32_t pin);
inline void set_pin_low(uint32_t pin);

inline uint8_t get_pin(uint32_t pin);




#endif
