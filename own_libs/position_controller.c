#include "stm32f4xx_conf.h"
#include "stm32f4xx.h"
#include "gpio_helper.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "position_controller.h"


/*	===============================================
* 	Set PWM speed according to the following rules:
* 	820  --> max reverse speed (CCW)
* 	4096 --> zero speed (motor forces to stand still)
* 	7372 --> max forward speed (CW)
* 	===============================================
*/

#include "own_libs/defines.h"
//

#define pi 3.14159265359

////// PID Controller //////
// Errors for MCU1 and MCU2
float previous_error_1 = 0, previous_error_2 = 0;
int16_t motor_position_int_old_1 = -1, motor_position_int_old_2 = -1;

////// Hardware ///////
const uint8_t GEAR_RATIO = 161;

////// Data transfer //////
// new_packet
uint8_t new_packet = 0;
uint8_t counter_old = 13;
uint8_t iterator1 = 0;
uint8_t iterator2 = 0;
// mapping parameters master slave
const float OUT_MIN_MS = -pi;
const float OUT_MAX_MS = pi;
const float IN_MIN_MS = 0;
const float IN_MAX_MS = 65536;

// motor constants
const float MAX_MOTOR_RPM = 3500.; // [rpm]
const float MAX_MOTOR_RAD_S = 3500. / 60. * 2 * pi; // [rad/s]
const float CONVERSION_COEFF = 60. / (2.*pi); // [RPM 1/(rad/s) ] v_rpm = CONVERSION_COEFF * v_rad_s

// mapping parameters pwm
const float OUT_MIN_PWM = 820;
const float OUT_MAX_PWM = 7372;
const float IN_MIN_PWM = -3500. / 60.*2.*pi;
const float IN_MAX_PWM = 3500. / 60.*2.*pi;

// constants related to data transfer
const uint16_t POS_FREQUENCY = POS_CONTROLL_FREQ;
const float DT = 1. / POS_CONTROLL_FREQ;


const uint16_t POS_VALUES = POS_CONTROLL_VAL_COUNT;



/* /Testing/ */
#if TESTING_ARRAY
	const uint16_t SLOW_POS_VALUES = 62;//63;
	const uint16_t SLOW_LOOP_VALUE = 20;
	int16_t slow_loop = 0;
	int16_t slow_iterator = 0;
	// 10ï¿½*sin() + 10ï¿½
	uint16_t POS[] = {36409,36772,37132,37485,37827,38154,38465,38754,39021,39261,39473,39654,39802,39917,39997,40041,40048,40019,39955,39854,39720,39552,39353,39124,38868,38588,38286,37965,37629,37280,36923,36560,36196,35835,35478,35132,34798,34480,34181,33905,33653,33430,33236,33073,32944,32850,32791,32768,32782,32832,32918,33038,33192,33379,33595,33840,34111,34404,34717,35048,35392,35746,36106};
	// 20ï¿½*sin() + 20ï¿½
	// uint_16 POS[] = {40050,40777,41496,42202,42885,43541,44161,44741,45273,45754,46177,46539,46837,47066,47226,47313,47328,47271,47141,46941,46671,46335,45937,45480,44968,44408,43804,43162,42489,41792,41077,40353,39625,38901,38189,37495,36827,36192,35594,35042,34539,34091,33703,33378,33120,32932,32814,32769,32796,32896,33067,33308,33617,33989,34423,34912,35453,36040,36667,37327,38015,38723,39445};
#endif
/* /Testing/ */




uint16_t position_controll_mcu(Master_Data *master_data, Slave_Data *slave_data, int mcu_origin)
{
	init_IMU(slave_data); // set IMU data to 0 so that they can used for safestate call

	#if CHECK_MCU_ENABLES
		disable_MCU(master_data->MCU1_POS[1],master_data->MCU2_POS[1]); // use second element of motor array to check if motor is enabled
	#endif

	/* /Testing/ */
	#if TESTING_ARRAY
		master_data->COUNTER = 0;

		slow_loop--;


		iterator1 = 0;
		iterator2 = 0;

		if(slow_loop <= 0)
		{
			slow_loop = SLOW_LOOP_VALUE;
			slow_iterator++;
			if(slow_iterator>=SLOW_POS_VALUES)
			{
				slow_iterator = 0;
			}

			master_data->MCU1_POS[0] = POS[slow_iterator];
			master_data->MCU1_POS[1] = POS[slow_iterator+1];
			master_data->MCU1_POS[2] = POS[slow_iterator+2];
			master_data->MCU1_POS[3] = POS[slow_iterator+3];
			master_data->MCU1_POS[4] = POS[slow_iterator+4];

			master_data->MCU2_POS[0] = 38000;
			master_data->MCU2_POS[1] = 38000;
			master_data->MCU2_POS[2] = 38000;
			master_data->MCU2_POS[3] = 38000;
			master_data->MCU2_POS[4] = 38000;

			//dt = 1./POS_FREQUENCY; // eventually adjust depending on SLOW_LOOP_VALUE
		}
	#endif
	/* /Testing/ */




	// differentiating slaves and mcus
	float joint_angle;
	int8_t motor_direction;


	// check if new packet
	#if PACKAGE_CHECK
    	if (counter_old != master_data->COUNTER)
    	{
    		iterator1 = 0;
    		iterator2 = 0;
    	}
    	else if(mcu_origin == 1)
    	{

    		if (iterator1 + 2<POS_VALUES - 1)
    			iterator1++;// check bounds to avoid out of range array access during vcnt calculation
    		else iterator1 = POS_VALUES - 2; // master has to consider that last value is not used by the slave
    	}
    	else if(mcu_origin == 2)
    	{

    		if (iterator2 + 2<POS_VALUES - 1)
    			iterator2++;// check bounds to avoid out of range array access during vcnt calculation
    		else iterator2 = POS_VALUES - 2; // master has to consider that last value is not used by the slave
    	}
	#else
    	// use only first value of array
		iterator1 = 0;
		iterator2 = 0;
	#endif
	// end check if new packet


	uint16_t new_pwm_speed1 = 0;
	uint16_t new_pwm_speed2 = 0;

	#if CONTROL_FILTER
		if(mcu_origin==1) // just filter once
		{
			#if (SLAVE_ID == 1 || SLAVE_ID == 3)
				slave_data->SOFT_POT_1 = filtering(slave_data->SOFT_POT_1,1);
				slave_data->SOFT_POT_2 = filtering(slave_data->SOFT_POT_2,2);
				slave_data->SOFT_POT_3 = filtering(slave_data->SOFT_POT_3,3);
			#elif SLAVE_ID == 2
				slave_data->HIP_POT_1 = filtering(slave_data->HIP_POT_1,1);
				slave_data->HIP_POT_2 = filtering(slave_data->HIP_POT_2,2);
			#endif
		}
	#endif



	#if SLAVE_ID == 1

			// Call the standard PID controller with variables for our problem

			//MCU 1
			#if USE_KNEE_FOR_LEVER
				joint_angle = LeftKneePot2Rad(slave_data->SOFT_POT_1);
				motor_direction = 1; // "-1" for right side; left "1"
				new_pwm_speed1 = PID_control_MCU(KP_11, KI_11, KD_11, K_marius_11, &previous_error_1, &motor_position_int_old_1, master_data->MCU1_POS, joint_angle, slave_data->SOFT_POT_1, motor_direction, iterator1);
			#else
				joint_angle = LeftLeverPot2Rad(slave_data->SOFT_POT_2);
				motor_direction = 1; // "-1" for right side; left "1"
				new_pwm_speed1 = PID_control_MCU(KP_11, KI_11, KD_11, K_marius_11, &previous_error_1, &motor_position_int_old_1, master_data->MCU1_POS, joint_angle, slave_data->SOFT_POT_2, motor_direction, iterator1);
			#endif

			//MCU 2
			joint_angle = LeftWinchPot2Rad(slave_data->SOFT_POT_3);
			motor_direction = 1; // "-1" for right side; left "1"
			new_pwm_speed2 = PID_control_MCU(KP_12, KI_12, KD_12, K_marius_12, &previous_error_2, &motor_position_int_old_2, master_data->MCU2_POS, joint_angle, slave_data->SOFT_POT_3, motor_direction, iterator2);

			// Safety Check
			#if SAFETY_ON
				// lever
				new_pwm_speed1 = safety_check(slave_data, new_pwm_speed1, master_data->MCU1_POS, iterator1, LeftLeverPot2Rad(slave_data->SOFT_POT_2), slave_data->SOFT_POT_2, LEVER_LEFT_MIN_VIRTUAL_1, LEVER_LEFT_MIN_VIRTUAL_2, LEVER_LEFT_MAX_VIRTUAL_1, LEVER_LEFT_MAX_VIRTUAL_2, LEVER_LEFT_MIN_VIRTUAL_1_RAD, LEVER_LEFT_MIN_VIRTUAL_2_RAD, LEVER_LEFT_MAX_VIRTUAL_1_RAD, LEVER_LEFT_MAX_VIRTUAL_2_RAD);
				// knee
				#if SAFETY_KNEE_CHECK
					new_pwm_speed1 = safety_check(slave_data, new_pwm_speed1, master_data->MCU1_POS, iterator1, LeftKneePot2Rad(slave_data->SOFT_POT_1), slave_data->SOFT_POT_1, KNEE_LEFT_MIN_VIRTUAL_1, KNEE_LEFT_MIN_VIRTUAL_2, KNEE_LEFT_MAX_VIRTUAL_1, KNEE_LEFT_MAX_VIRTUAL_2, KNEE_LEFT_MIN_VIRTUAL_1_RAD, KNEE_LEFT_MIN_VIRTUAL_2_RAD, KNEE_LEFT_MAX_VIRTUAL_1_RAD, KNEE_LEFT_MAX_VIRTUAL_2_RAD);
				#endif
				// winch
				new_pwm_speed2 = safety_check(slave_data, new_pwm_speed2, master_data->MCU2_POS, iterator2, LeftWinchPot2Rad(slave_data->SOFT_POT_3), slave_data->SOFT_POT_3, WINCH_LEFT_MIN_VIRTUAL_1, WINCH_LEFT_MIN_VIRTUAL_2, WINCH_LEFT_MAX_VIRTUAL_1, WINCH_LEFT_MAX_VIRTUAL_2, WINCH_LEFT_MIN_VIRTUAL_1_RAD, WINCH_LEFT_MIN_VIRTUAL_2_RAD, WINCH_LEFT_MAX_VIRTUAL_1_RAD, WINCH_LEFT_MAX_VIRTUAL_2_RAD);
			#endif

	#elif SLAVE_ID == 2

			// Call the standard PID controller with variables for our problem

			//MCU 1
			joint_angle = LeftHipPot2Rad(slave_data->HIP_POT_1);
			motor_direction = 1; // "-1" for right side; left "1"
			new_pwm_speed1 = PID_control_MCU(KP_21, KI_21, KD_21, K_marius_21, &previous_error_1, &motor_position_int_old_1, master_data->MCU1_POS, joint_angle, slave_data->HIP_POT_1, motor_direction, iterator1);


			//MCU 2
			joint_angle = RightHipPot2Rad(slave_data->HIP_POT_2);
			motor_direction = -1; // "-1" for right side; left "1"
			new_pwm_speed2 = PID_control_MCU(KP_22, KI_22, KD_22, K_marius_22, &previous_error_2, &motor_position_int_old_2, master_data->MCU2_POS, joint_angle, slave_data->HIP_POT_2, motor_direction, iterator2);

			// Safety Check
			#if SAFETY_ON
				// left hip
				new_pwm_speed1 = safety_check(slave_data, new_pwm_speed1, master_data->MCU1_POS, iterator1, LeftHipPot2Rad(slave_data->HIP_POT_1), slave_data->HIP_POT_1, HIP_LEFT_MIN_VIRTUAL_1, HIP_LEFT_MIN_VIRTUAL_2, HIP_LEFT_MAX_VIRTUAL_1, HIP_LEFT_MAX_VIRTUAL_2, HIP_LEFT_MIN_VIRTUAL_1_RAD, HIP_LEFT_MIN_VIRTUAL_2_RAD, HIP_LEFT_MAX_VIRTUAL_1_RAD, HIP_LEFT_MAX_VIRTUAL_2_RAD);
				// right hip
				new_pwm_speed2 = safety_check(slave_data, new_pwm_speed2, master_data->MCU2_POS, iterator2, RightHipPot2Rad(slave_data->HIP_POT_2), slave_data->HIP_POT_2, HIP_RIGHT_MIN_VIRTUAL_1, HIP_RIGHT_MIN_VIRTUAL_2, HIP_RIGHT_MAX_VIRTUAL_1, HIP_RIGHT_MAX_VIRTUAL_2, HIP_RIGHT_MIN_VIRTUAL_1_RAD, HIP_RIGHT_MIN_VIRTUAL_2_RAD, HIP_RIGHT_MAX_VIRTUAL_1_RAD, HIP_RIGHT_MAX_VIRTUAL_2_RAD);
			#endif


	#elif SLAVE_ID == 3

			// Call the standard PID controller with variables for our problem

			//MCU 1
			#if USE_KNEE_FOR_LEVER
				joint_angle = RightKneePot2Rad(slave_data->SOFT_POT_1);
				motor_direction = -1; // "-1" for right side; left "1"
				new_pwm_speed1 = PID_control_MCU(KP_31, KI_31, KD_31, K_marius_11, &previous_error_1, &motor_position_int_old_1, master_data->MCU1_POS, joint_angle, slave_data->SOFT_POT_1, motor_direction, iterator1);
			#else
				joint_angle = RightLeverPot2Rad(slave_data->SOFT_POT_2);
				motor_direction = -1; // "-1" for right side; left "1"
				new_pwm_speed1 = PID_control_MCU(KP_31, KI_31, KD_31, K_marius_11, &previous_error_1, &motor_position_int_old_1, master_data->MCU1_POS, joint_angle, slave_data->SOFT_POT_2, motor_direction, iterator1);
			#endif

			//MCU 2
			joint_angle = RightWinchPot2Rad(slave_data->SOFT_POT_3);
			motor_direction = -1; // "-1" for right side; left "1"
			new_pwm_speed2 = PID_control_MCU(KP_32, KI_32, KD_32, K_marius_32, &previous_error_2, &motor_position_int_old_2, master_data->MCU2_POS, joint_angle, slave_data->SOFT_POT_3, motor_direction, iterator2);

			// Safety Check
			#if SAFETY_ON
				// lever
				new_pwm_speed1 = safety_check(slave_data, new_pwm_speed1, master_data->MCU1_POS, iterator1, RightLeverPot2Rad(slave_data->SOFT_POT_2), slave_data->SOFT_POT_2, LEVER_RIGHT_MIN_VIRTUAL_1, LEVER_RIGHT_MIN_VIRTUAL_2, LEVER_RIGHT_MAX_VIRTUAL_1, LEVER_RIGHT_MAX_VIRTUAL_2, LEVER_RIGHT_MIN_VIRTUAL_1_RAD, LEVER_RIGHT_MIN_VIRTUAL_2_RAD, LEVER_RIGHT_MAX_VIRTUAL_1_RAD, LEVER_RIGHT_MAX_VIRTUAL_2_RAD);
				// knee
				#if SAFETY_KNEE_CHECK
					new_pwm_speed1 = safety_check(slave_data, new_pwm_speed1, master_data->MCU1_POS, iterator1, RightKneePot2Rad(slave_data->SOFT_POT_1), slave_data->SOFT_POT_1, KNEE_RIGHT_MIN_VIRTUAL_1, KNEE_RIGHT_MIN_VIRTUAL_2, KNEE_RIGHT_MAX_VIRTUAL_1, KNEE_RIGHT_MAX_VIRTUAL_2, KNEE_RIGHT_MIN_VIRTUAL_1_RAD, KNEE_RIGHT_MIN_VIRTUAL_2_RAD, KNEE_RIGHT_MAX_VIRTUAL_1_RAD, KNEE_RIGHT_MAX_VIRTUAL_2_RAD);
				#endif
				// winch
				new_pwm_speed2 = safety_check(slave_data, new_pwm_speed2, master_data->MCU2_POS, iterator2, RightWinchPot2Rad(slave_data->SOFT_POT_3), slave_data->SOFT_POT_3, WINCH_RIGHT_MIN_VIRTUAL_1, WINCH_RIGHT_MIN_VIRTUAL_2, WINCH_RIGHT_MAX_VIRTUAL_1, WINCH_RIGHT_MAX_VIRTUAL_2, WINCH_RIGHT_MIN_VIRTUAL_1_RAD, WINCH_RIGHT_MIN_VIRTUAL_2_RAD, WINCH_RIGHT_MAX_VIRTUAL_1_RAD, WINCH_RIGHT_MAX_VIRTUAL_2_RAD);
			#endif

	#endif
	// Packet counter
	counter_old = master_data->COUNTER;

	if(mcu_origin == 1)
		return new_pwm_speed1;
	else if(mcu_origin == 2)
		return new_pwm_speed2;
	else
		return 4096;
}



uint16_t PID_control_MCU(float KP, float KI, float KD, float K_marius, float* previous_error, int* motor_position_int_old, const uint16_t MCU_POS[POS_CONTROLL_VAL_COUNT], float joint_angle, const uint16_t joint_angle_raw, const int8_t motor_direction, const  uint8_t iterator)
{
	// Initialisation
	uint16_t new_pwm_speed = 0;
	float new_motor_speed = 0;

	// access motor value
	int motor_position_int = MCU_POS[iterator];
	int motor_position_int_1 = MCU_POS[iterator + 1];

	// inverse mapping as "map" in control_functions.cpp
	float motor_position = (motor_position_int - IN_MIN_MS) * (OUT_MAX_MS - OUT_MIN_MS) / (IN_MAX_MS - IN_MIN_MS) + OUT_MIN_MS; // [rad]
	float motor_position_1 = (motor_position_int_1 - IN_MIN_MS) * (OUT_MAX_MS - OUT_MIN_MS) / (IN_MAX_MS - IN_MIN_MS) + OUT_MIN_MS; // [rad]



	////// PID Controller ///////

	// calculate proportional error
	float error = motor_position - joint_angle;

	// calculate derivative error
	float derivative = (error - *previous_error) * POS_FREQUENCY;

	/* // for hip and pretension:
	* if(motor_position_int_old != -1)
	* {
	* 		derivative = (motor_position_int -motor_position_int_old)/dt - mcu1_rpm;
	* }
	* else
	* {
	* 		derivative = 0;
	* }
	*/

	// calculate integral error
	float integral = integral + error*DT;

	// Sum PID errors
	float PID_output = KP*error + KI*integral + KD*derivative;

	// end PID Controller

	// velocity feedforward
	float vcnt = (motor_position_1 - motor_position) * POS_FREQUENCY;

	/* Testing */
	// PID Parameters
	// vcnt = 0;
	/* Testing */

	// sum PID and velocity feedforward
	new_motor_speed = PID_output + K_marius*vcnt;


	// gear transmisson ratio
	new_motor_speed = motor_direction * new_motor_speed * GEAR_RATIO; // "-new_motor_speed" for right side; left "new_motor_speed"

	// Check motor speed bounds in rad/s
	if (new_motor_speed > MAX_MOTOR_RAD_S)
		new_motor_speed = MAX_MOTOR_RAD_S;
	if (new_motor_speed < -MAX_MOTOR_RAD_S)
		new_motor_speed = -MAX_MOTOR_RAD_S;


	// tranformation rad/s to pwm
	new_pwm_speed = (int)((new_motor_speed - IN_MIN_PWM) * (OUT_MAX_PWM - OUT_MIN_PWM) / (IN_MAX_PWM - IN_MIN_PWM) + OUT_MIN_PWM);

	// store old values

	// PID controller
	*previous_error = error;

	// Reference position
	*motor_position_int_old = motor_position_int;

	// a posteriori anti-windup
	if (new_motor_speed >= MAX_MOTOR_RAD_S)
		integral = integral - error*DT;


	// Check motor speed bounds in terms of pwm signal
	if (new_pwm_speed > 7372)
		new_pwm_speed = 7372;
	if (new_pwm_speed < 820)
		new_pwm_speed = 820;

	return new_pwm_speed;
}


uint16_t safety_check( Slave_Data *slave_data,uint16_t new_pwm_speed,const uint16_t MCU_POS[POS_CONTROLL_VAL_COUNT], const  uint8_t iterator ,float joint_angle, const uint16_t joint_angle_raw, const uint16_t min_virtual_1, const uint16_t min_virtual_2, const uint16_t max_virtual_1, const uint16_t max_virtual_2, const float min_virtual_1_rad, const float min_virtual_2_rad, const float max_virtual_1_rad, const float max_virtual_2_rad)
{
	// access motor value
	int motor_position_int = MCU_POS[iterator];
	// inverse mapping as "map" in control_functions.cpp
	float motor_position = (motor_position_int - IN_MIN_MS) * (OUT_MAX_MS - OUT_MIN_MS) / (IN_MAX_MS - IN_MIN_MS) + OUT_MIN_MS; // [rad]


	////// Safety ///////

	// check if sensor values make sense: do not jump by more than XX --> unplugged sensor --> rpm = 0
	#if SAFETY_SENSOR_VALUES

	#endif

	// check if send motor values do not violate joint limits --> USB Communication problem --> rpm = 0
	#if SAFETY_INPUT_VALUES
		if(motor_position<min_virtual_1_rad)
			new_pwm_speed = 4096;
		if(motor_position>max_virtual_1_rad)
			new_pwm_speed = 4096;
	#endif

	// check if sensor values indicate motor is in slightly critical region --> rpm slow away from virtual stopper 1
	#if SAFETY_VIRTUAL_STOPPERS_1
		if (joint_angle_raw < min_virtual_1)
			new_pwm_speed = 4096+75;
		if (joint_angle_raw > max_virtual_1)
			new_pwm_speed = 4096-75;

	#endif

	// check if sensor values indicate motor is in significantly critical region  --> rpm = 0 and enable safe state
	#if SAFETY_VIRTUAL_STOPPERS_2
		if (joint_angle_raw < min_virtual_2)
		{
			new_pwm_speed = 4096;
			call_safe_state(slave_data);
		}
		if (joint_angle_raw > max_virtual_2)
		{
			new_pwm_speed = 4096;
			call_safe_state(slave_data);
		}

	#endif

	return new_pwm_speed;
}


void call_safe_state( Slave_Data *slave_data)
{
	#if SAFE_STATE
		set_pin_low(GPIO_Pin_0);
		set_pin_low(GPIO_Pin_1);
		slave_data->IMU1_AX = 1;
		slave_data->IMU1_AY = 1;
		slave_data->IMU1_AZ = 1;
		slave_data->IMU1_GX = 1;
		slave_data->IMU1_GY = 1;
		slave_data->IMU1_GZ = 1;
		slave_data->IMU2_AX = 1;
		slave_data->IMU2_AY = 1;
		slave_data->IMU2_AZ = 1;
		slave_data->IMU2_GX = 1;
		slave_data->IMU2_GY = 1;
		slave_data->IMU2_GZ = 1;
		slave_data->IMU3_AX = 1;
		slave_data->IMU3_AY = 1;
		slave_data->IMU3_AZ = 1;
		slave_data->IMU3_GX = 1;
		slave_data->IMU3_GY = 1;
		slave_data->IMU3_GZ = 1;
	#endif
}

void init_IMU(Slave_Data *slave_data)
{
	slave_data->IMU1_AX = 0;
	slave_data->IMU1_AY = 0;
	slave_data->IMU1_AZ = 0;
	slave_data->IMU1_GX = 0;
	slave_data->IMU1_GY = 0;
	slave_data->IMU1_GZ = 0;
	slave_data->IMU2_AX = 0;
	slave_data->IMU2_AY = 0;
	slave_data->IMU2_AZ = 0;
	slave_data->IMU2_GX = 0;
	slave_data->IMU2_GY = 0;
	slave_data->IMU2_GZ = 0;
	slave_data->IMU3_AX = 0;
	slave_data->IMU3_AY = 0;
	slave_data->IMU3_AZ = 0;
	slave_data->IMU3_GX = 0;
	slave_data->IMU3_GY = 0;
	slave_data->IMU3_GZ = 0;
}

#define MEMORYSIZE 3
float data1[MEMORYSIZE]={0,0,0};
float data2[MEMORYSIZE]={0,0,0};
float data3[MEMORYSIZE]={0,0,0};
float fdata1[MEMORYSIZE]={0,0,0};
float fdata2[MEMORYSIZE]={0,0,0};
float fdata3[MEMORYSIZE]={0,0,0};

uint16_t filtering(uint16_t new_data,uint16_t sensor)
{
	float a[] = { 1.000, 0 , 0.1716 };
	float b[] = { 0.2929 , 0.5858 , 0.2929 };

	// copy to local variables

	float data[MEMORYSIZE]={0,0,0};
	float fdata[MEMORYSIZE]={0,0,0};
	uint16_t k;
	for (k = 0; k < MEMORYSIZE; ++k)
	{
		if(sensor == 1)
		{
			data1[0]=new_data;
			data[k]=data1[k];
			fdata[k]=fdata1[k];
		}
		if(sensor == 2)
		{
			data2[0]=new_data;
			data[k]=data2[k];
			fdata[k]=fdata2[k];
		}
		if(sensor == 3)
		{
			data3[0]=new_data;
			data[k]=data3[k];
			fdata[k]=fdata3[k];
		}
	}

	// do some filtering


	float B_term = 0;
	float A_term = 0;
	for (k = 0; k < MEMORYSIZE; ++k)
		B_term += b[k] * data[k];
	for (k = 1; k < MEMORYSIZE; ++k)
	{
		A_term += a[k] * fdata[k];
		//fdata[k] = fdata[k - 1];
	}
	fdata[0] = 1. / a[0] * (B_term - A_term);

	// copy to global variables

	for (k = 1; k < MEMORYSIZE; ++k)
	{
		if(sensor == 1)
		{
			fdata1[0]=fdata[0];
			fdata1[k]=fdata[k-1];
			data1[k]=data[k-1];
		}
		if(sensor == 2)
		{
			fdata2[0]=fdata[0];
			fdata2[k]=fdata[k-1];
			data2[k]=data[k-1];
		}
		if(sensor == 3)
		{
			fdata3[0]=fdata[0];
			fdata3[k]=fdata[k-1];
			data3[k]=data[k-1];
		}
	}
	return (uint16_t)fdata[0];
}

void disable_MCU(const uint16_t MCU1_POS1,const uint16_t MCU2_POS1)
{
	if(MCU1_POS1 == 1)
	{
		set_pin_high(GPIO_Pin_0);
	}
	else
	{
		set_pin_low(GPIO_Pin_0);
	}

	if(MCU2_POS1 == 1)
	{
		set_pin_high(GPIO_Pin_1);
	}
	else
	{
		set_pin_low(GPIO_Pin_1);
	}
}