#ifndef __IMU_HELPER_H__
#define __IMU_HELPER_H__

void init_imu_helper(){
	I2C_InitTypeDef I2C_InitStruct;
	GPIO_InitTypeDef GPIO_I2C_InitStruct;


	/* Set default I2C GPIO	settings */
	GPIO_I2C_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_I2C_InitStruct.GPIO_OType = GPIO_OType_OD;
	GPIO_I2C_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_I2C_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;

	/* Enable clock */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);

	/* Enable pins */
	/* Enable clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);


	//                      		SCL          SDA
	GPIO_I2C_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;

	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_I2C1);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_I2C1);

	/* Init pins */
	GPIO_Init(GPIOB, &GPIO_I2C_InitStruct);

	/* Set values */
	I2C_InitStruct.I2C_ClockSpeed = 100000;
	I2C_InitStruct.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStruct.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStruct.I2C_OwnAddress1 = 0x00;
	I2C_InitStruct.I2C_Ack = I2C_Ack_Disable;
	I2C_InitStruct.I2C_DutyCycle = I2C_DutyCycle_2;


	/* Initialize I2C */
	I2C_Init(I2C1, &I2C_InitStruct);
	/* Enable I2C */
	I2C_Cmd(I2C1, ENABLE);

}
#endif
