#include "stm32f4xx_conf.h"
#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_adc.h"


#ifndef __USB_HELPER_H__
#define __USB_HELPER_H__

void ADCH_init(uint32_t ADCx, uint32_t pin_header, uint16_t pin, uint8_t adc_channel);

uint16_t ADCH_getValue(ADC_TypeDef* adc_type, uint8_t channel);
uint16_t ADCH_getValueX(ADC_TypeDef* adc_type, uint8_t channel, uint8_t sample_time);


#endif
