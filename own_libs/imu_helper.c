#include "tm_stm32f4_i2c.h"
#include "imu_helper.h"
//#include "IMU_struct.c"


void init_imu(IMU_struct s){
	TM_I2C_Init(s.I2C_type, s.pins, s.speed);
	initMag(s);
	initAc(s);
	initGyro(s);
}

void update_gyro_acc(IMU_struct* s) {
	readAc(s);
	readGyro(s);
}


void fill_imu_data(Slave_Data *slave_data, IMU_struct *s)
{
	switch(s->ID)
	{
		case 1:
			slave_data->IMU1_AX = s->ax;
			slave_data->IMU1_AY = s->ay;
			slave_data->IMU1_AZ = s->az;
			slave_data->IMU1_GX = s->gx;
			slave_data->IMU1_GY = s->gy;
			slave_data->IMU1_GZ = s->gz;
			break;
		case 2:
			slave_data->IMU2_AX = s->ax;
			slave_data->IMU2_AY = s->ay;
			slave_data->IMU2_AZ = s->az;
			slave_data->IMU2_GX = s->gx;
			slave_data->IMU2_GY = s->gy;
			slave_data->IMU2_GZ = s->gz;
			break;
		case 3:
			slave_data->IMU3_AX = s->ax;
			slave_data->IMU3_AY = s->ay;
			slave_data->IMU3_AZ = s->az;
			slave_data->IMU3_GX = s->gx;
			slave_data->IMU3_GY = s->gy;
			slave_data->IMU3_GZ = s->gz;
			break;
		default:
			break;
	}
}


void initMag(IMU_struct s) {

	/* CTRL_REG5_XM enables temp sensor, sets mag resolution and data rate
	 Bits (7-0): TEMP_EN M_RES1 M_RES0 M_ODR2 M_ODR1 M_ODR0 LIR2 LIR1
	 TEMP_EN - Enable temperature sensor (0=disabled, 1=enabled)
	 M_RES[1:0] - Magnetometer resolution select (0=low, 3=high)
	 M_ODR[2:0] - Magnetometer data rate select
	 000=3.125Hz, 001=6.25Hz, 010=12.5Hz, 011=25Hz, 100=50Hz, 101=100Hz
	 LIR2 - Latch interrupt request on INT2_SRC (cleared by reading INT2_SRC)
	 0=interrupt request not latched, 1=interrupt request latched
	 LIR1 - Latch interrupt request on INT1_SRC (cleared by readging INT1_SRC)
	 0=irq not latched, 1=irq latched 									 */
	TM_I2C_Write(s.I2C_type, s.ADDRESS_A, CTRL_REG5_XM, 0x94); // Mag data rate - 100 Hz, enable temperature sensor

	/* CTRL_REG6_XM sets the magnetometer full-scale
	 Bits (7-0): 0 MFS1 MFS0 0 0 0 0 0
	 MFS[1:0] - Magnetic full-scale selection
	 00:+/-2Gauss, 01:+/-4Gs, 10:+/-8Gs, 11:+/-12Gs							 */
	TM_I2C_Write(s.I2C_type, s.ADDRESS_A, CTRL_REG6_XM, 0x00); // Mag scale to +/- 2GS

	/* CTRL_REG7_XM sets magnetic sensor mode, low power mode, and filters
	 AHPM1 AHPM0 AFDS 0 0 MLP MD1 MD0
	 AHPM[1:0] - HPF mode selection
	 00=normal (resets reference registers), 01=reference signal for filtering,
	 10=normal, 11=autoreset on interrupt event
	 AFDS - Filtered acceleration data selection
	 0=internal filter bypassed, 1=data from internal filter sent to FIFO
	 MLP - Magnetic data low-power mode
	 0=data rate is set by M_ODR bits in CTRL_REG5
	 1=data rate is set to 3.125Hz
	 MD[1:0] - Magnetic sensor mode selection (default 10)
	 00=continuous-conversion, 01=single-conversion, 10 and 11=power-down */
	TM_I2C_Write(s.I2C_type, s.ADDRESS_A, CTRL_REG7_XM, 0x00); // Continuous conversion mode

	/* CTRL_REG4_XM is used to set interrupt generators on INT2_XM
	 Bits (7-0): P2_TAP P2_INT1 P2_INT2 P2_INTM P2_DRDYA P2_DRDYM P2_Overrun P2_WTM
	 */
	TM_I2C_Write(s.I2C_type, s.ADDRESS_A, CTRL_REG4_XM, 0x04); // Magnetometer data ready on INT2_XM (0x08)

	/* INT_CTRL_REG_M to set push-pull/open drain, and active-low/high
	 Bits[7:0] - XMIEN YMIEN ZMIEN PP_OD IEA IEL 4D MIEN
	 XMIEN, YMIEN, ZMIEN - Enable interrupt recognition on axis for mag data
	 PP_OD - Push-pull/open-drain interrupt configuration (0=push-pull, 1=od)
	 IEA - Interrupt polarity for accel and magneto
	 0=active-low, 1=active-high
	 IEL - Latch interrupt request for accel and magneto
	 0=irq not latched, 1=irq latched
	 4D - 4D enable. 4D detection is enabled when 6D bit in INT_GEN1_REG is set
	 MIEN - Enable interrupt generation for magnetic data
	 0=disable, 1=enable) */
	TM_I2C_Write(s.I2C_type, s.ADDRESS_A, INT_CTRL_REG_M, 0x09); // Enable interrupts for mag, active-low, push-pull

}


void readMag(IMU_struct* s) {

	int16_t mxx = 0;
	int16_t myy = 0;
	int16_t mzz = 0;

	uint8_t temp[6];

	temp[0] = TM_I2C_Read(s->I2C_type, s->ADDRESS_A, OUT_X_L_M);
	temp[1] = TM_I2C_Read(s->I2C_type, s->ADDRESS_A, OUT_X_H_M);
	temp[2] = TM_I2C_Read(s->I2C_type, s->ADDRESS_A, OUT_Y_L_M);
	temp[3] = TM_I2C_Read(s->I2C_type, s->ADDRESS_A, OUT_Y_H_M);
	temp[4] = TM_I2C_Read(s->I2C_type, s->ADDRESS_A, OUT_Z_L_M);
	temp[5] = TM_I2C_Read(s->I2C_type, s->ADDRESS_A, OUT_Z_H_M);

	mxx = (temp[1] << 8) | temp[0];
	myy = (temp[3] << 8) | temp[2];
	mzz = (temp[5] << 8) | temp[4];

	s->mx = mxx * s->mRes;
	s->my = myy * s->mRes;
	s->mz = mzz * s->mRes;

}

void readTemperature(IMU_struct* s) {
	int16_t t = 0;

	int8_t temp[2];
	temp[0] = TM_I2C_Read(s->I2C_type, s->ADDRESS_A, OUT_TEMP_L_XM);
	temp[1] = TM_I2C_Read(s->I2C_type, s->ADDRESS_A, OUT_TEMP_H_XM);
	t = (((int16_t) temp[1] << 12) | temp[0] << 4 ) >> 4;
	s->temp = 21.0 + (float) t / 8.;
}

void initGyro(IMU_struct s) {
	/* CTRL_REG1_G sets output data rate, bandwidth, power-down and enables
	 Bits[7:0]: DR1 DR0 BW1 BW0 PD Zen Xen Yen
	 DR[1:0] - Output data rate selection
	 00=95Hz, 01=190Hz, 10=380Hz, 11=760Hz
	 BW[1:0] - Bandwidth selection (sets cutoff frequency)
	 Value depends on ODR. See datasheet table 21.
	 PD - Power down enable (0=power down mode, 1=normal or sleep mode)
	 Zen, Xen, Yen - Axis enable (o=disabled, 1=enabled)	*/
	TM_I2C_Write(s.I2C_type, s.ADDRESS_G, CTRL_REG1_G, 0x0F); // Normal mode, enable all axes

	/* CTRL_REG2_G sets up the HPF
	 Bits[7:0]: 0 0 HPM1 HPM0 HPCF3 HPCF2 HPCF1 HPCF0
	 HPM[1:0] - High pass filter mode selection
	 00=normal (reset reading HP_RESET_FILTER, 01=ref signal for filtering,
	 10=normal, 11=autoreset on interrupt
	 HPCF[3:0] - High pass filter cutoff frequency
	 Value depends on data rate. See datasheet table 26.
	 */
	TM_I2C_Write(s.I2C_type, s.ADDRESS_G, CTRL_REG2_G, 0x00); // Normal mode, high cutoff frequency

	/* CTRL_REG3_G sets up interrupt and DRDY_G pins
	 Bits[7:0]: I1_IINT1 I1_BOOT H_LACTIVE PP_OD I2_DRDY I2_WTM I2_ORUN I2_EMPTY
	 I1_INT1 - Interrupt enable on INT_G pin (0=disable, 1=enable)
	 I1_BOOT - Boot status available on INT_G (0=disable, 1=enable)
	 H_LACTIVE - Interrupt active configuration on INT_G (0:high, 1:low)
	 PP_OD - Push-pull/open-drain (0=push-pull, 1=open-drain)
	 I2_DRDY - Data ready on DRDY_G (0=disable, 1=enable)
	 I2_WTM - FIFO watermark interrupt on DRDY_G (0=disable 1=enable)
	 I2_ORUN - FIFO overrun interrupt on DRDY_G (0=disable 1=enable)
	 I2_EMPTY - FIFO empty interrupt on DRDY_G (0=disable 1=enable) */
	// Int1 enabled (pp, active low), data read on DRDY_G:
	TM_I2C_Write(s.I2C_type, s.ADDRESS_G, CTRL_REG3_G, 0x88); // Int1 enabled (pp, active low), data read on DRDY_G:

	/* CTRL_REG4_G sets the scale, update mode
	 Bits[7:0] - BDU BLE FS1 FS0 - ST1 ST0 SIM
	 BDU - Block data update (0=continuous, 1=output not updated until read
	 BLE - Big/little endian (0=data LSB @ lower address, 1=LSB @ higher add)
	 FS[1:0] - Full-scale selection
	 00=245dps, 01=500dps, 10=2000dps, 11=2000dps
	 ST[1:0] - Self-test enable
	 00=disabled, 01=st 0 (x+, y-, z-), 10=undefined, 11=st 1 (x-, y+, z+)
	 SIM - SPI serial interface mode select
	 0=4 wire, 1=3 wire */
	TM_I2C_Write(s.I2C_type, s.ADDRESS_G, CTRL_REG4_G, 0x10); // Set scale to 245 dps

	/* CTRL_REG5_G sets up the FIFO, HPF, and INT1
	 Bits[7:0] - BOOT FIFO_EN - HPen INT1_Sel1 INT1_Sel0 Out_Sel1 Out_Sel0
	 BOOT - Reboot memory content (0=normal, 1=reboot)
	 FIFO_EN - FIFO enable (0=disable, 1=enable)
	 HPen - HPF enable (0=disable, 1=enable)
	 INT1_Sel[1:0] - Int 1 selection configuration
	 Out_Sel[1:0] - Out selection configuration */
	TM_I2C_Write(s.I2C_type, s.ADDRESS_G, CTRL_REG5_G, 0x00); //CTRL_REG5_G sets up the FIFO, HPF, and INT1

}
void readGyro(IMU_struct* s) {

	int16_t gxx = 0;
	int16_t gyy = 0;
	int16_t gzz = 0;

	uint8_t temp[6];

	//TM_I2C_ReadMulti(I2C1, ADDRESS, OUT_X_L_G, &temp[0], 6); // Read 6 bytes, beginning at OUT_X_L_G
	temp[0] = TM_I2C_Read(s->I2C_type, s->ADDRESS_G, OUT_X_L_G);
	temp[1] = TM_I2C_Read(s->I2C_type, s->ADDRESS_G, OUT_X_H_G);
	temp[2] = TM_I2C_Read(s->I2C_type, s->ADDRESS_G, OUT_Y_L_G);
	temp[3] = TM_I2C_Read(s->I2C_type, s->ADDRESS_G, OUT_Y_H_G);
	temp[4] = TM_I2C_Read(s->I2C_type, s->ADDRESS_G, OUT_Z_L_G);
	temp[5] = TM_I2C_Read(s->I2C_type, s->ADDRESS_G, OUT_Z_H_G);

	gxx = (temp[1] << 8) | temp[0]; // Store x-axis values into gxx
	gyy = (temp[3] << 8) | temp[2]; // Store y-axis values into gyy
	gzz = (temp[5] << 8) | temp[4]; // Store z-axis values into gzz


	//s->gx = s->gRes * gxx;
	//s->gy = s->gRes * gyy;
	//s->gz = s->gRes * gzz;

	s->gx = gxx;
	s->gy = gyy;
	s->gz = gzz;
}


void initAc(IMU_struct s) {
	/* CTRL_REG0_XM (0x1F) (Default value: 0x00)
	 Bits (7-0): BOOT FIFO_EN WTM_EN 0 0 HP_CLICK HPIS1 HPIS2
	 BOOT - Reboot memory content (0: normal, 1: reboot)
	 FIFO_EN - Fifo enable (0: disable, 1: enable)
	 WTM_EN - FIFO watermark enable (0: disable, 1: enable)
	 HP_CLICK - HPF enabled for click (0: filter bypassed, 1: enabled)
	 HPIS1 - HPF enabled for interrupt generator 1 (0: bypassed, 1: enabled)
	 HPIS2 - HPF enabled for interrupt generator 2 (0: bypassed, 1 enabled)   */
	TM_I2C_Write(s.I2C_type, s.ADDRESS_A, CTRL_REG0_XM, 0x00);

	/* CTRL_REG1_XM (0x20) (Default value: 0x07)
	 Bits (7-0): AODR3 AODR2 AODR1 AODR0 BDU AZEN AYEN AXEN
	 AODR[3:0] - select the acceleration data rate:
	 0000=power down, 0001=3.125Hz, 0010=6.25Hz, 0011=12.5Hz,
	 0100=25Hz, 0101=50Hz, 0110=100Hz, 0111=200Hz, 1000=400Hz,
	 1001=800Hz, 1010=1600Hz, (remaining combinations undefined).
	 BDU - block data update for accel AND mag
	 0: Continuous update
	 1: Output registers aren't updated until MSB and LSB have been read.
	 AZEN, AYEN, and AXEN - Acceleration x/y/z-axis enabled.
	 0: Axis disabled, 1: Axis enabled									 */
	TM_I2C_Write(s.I2C_type, s.ADDRESS_A, CTRL_REG1_XM, 0x57); // 100Hz data rate, x/y/z all enabled

	/* CTRL_REG2_XM (0x21) (Default value: 0x00)
	 Bits (7-0): ABW1 ABW0 AFS2 AFS1 AFS0 AST1 AST0 SIM
	 ABW[1:0] - Accelerometer anti-alias filter bandwidth
	 00=773Hz, 01=194Hz, 10=362Hz, 11=50Hz
	 AFS[2:0] - Accel full-scale selection
	 000=+/-2g, 001=+/-4g, 010=+/-6g, 011=+/-8g, 100=+/-16g
	 AST[1:0] - Accel self-test enable
	 00=normal (no self-test), 01=positive st, 10=negative st, 11=not allowed
	 SIM - SPI mode selection
	 0=4-wire, 1=3-wire													 */
	TM_I2C_Write(s.I2C_type, s.ADDRESS_A, CTRL_REG2_XM, 0x18); // Set scale to 2g

	/* CTRL_REG3_XM is used to set interrupt generators on INT1_XM
	 Bits (7-0): P1_BOOT P1_TAP P1_INT1 P1_INT2 P1_INTM P1_DRDYA P1_DRDYM P1_EMPTY
	 */
	// Accelerometer data ready on INT1_XM (0x04)
	TM_I2C_Write(s.I2C_type, s.ADDRESS_A, CTRL_REG3_XM, 0x04);

}

void readAc(IMU_struct* s) {

	int16_t axx = 0;
	int16_t ayy = 0;
	int16_t azz = 0;

	uint8_t temp[6];

	//TM_I2C_ReadMulti(s->I2C_type, s->ADDRESS_A, OUT_X_L_A, &temp[0], 6); // Read 6 bytes, beginning at OUT_X_L_G
	temp[0] = TM_I2C_Read(s->I2C_type, s->ADDRESS_A, OUT_X_L_A);
	temp[1] = TM_I2C_Read(s->I2C_type, s->ADDRESS_A, OUT_X_H_A);
	temp[2] = TM_I2C_Read(s->I2C_type, s->ADDRESS_A, OUT_Y_L_A);
	temp[3] = TM_I2C_Read(s->I2C_type, s->ADDRESS_A, OUT_Y_H_A);
	temp[4] = TM_I2C_Read(s->I2C_type, s->ADDRESS_A, OUT_Z_L_A);
	temp[5] = TM_I2C_Read(s->I2C_type, s->ADDRESS_A, OUT_Z_H_A);

	axx = (temp[1] << 8) | temp[0];
	ayy = (temp[3] << 8) | temp[2];
	azz = (temp[5] << 8) | temp[4];

	//s->ax = axx * s->aRes;
	//s->ay = ayy * s->aRes;
	//s->az = azz * s->aRes;


	s->ax = axx;
	s->ay = ayy;
	s->az = azz;
}
