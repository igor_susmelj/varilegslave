#ifndef __USB_HELPER_H__
#define __USB_HELPER_H__

void init_usb();

void OTG_FS_IRQHandler(void);
void OTG_FS_WKUP_IRQHandler(void);

#endif
