
#include "adc_helper.h"


void ADCH_init(uint32_t ADCx, uint32_t pin_header, uint16_t pin, uint8_t adc_channel){
	//configure ADC
			//RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	switch(ADCx)
	{
		case (uint32_t)ADC1:
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
			break;
		case (uint32_t)ADC2:
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC2, ENABLE);
			break;
		case (uint32_t)ADC3:
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC3, ENABLE);
			break;
	}
			//RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

	switch(pin_header)
	{
		case (uint32_t)GPIOA:
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
			break;
		case (uint32_t)GPIOB:
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
			break;
		case (uint32_t)GPIOC:
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
			break;
		case (uint32_t)GPIOD:
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
			break;
	}
	//RCC_AHB1PeriphClockCmd(pin_header, ENABLE);

	GPIO_InitTypeDef ADC_Config;
	GPIO_StructInit(&ADC_Config);

	//configure pin
			//ADC_Config.GPIO_Pin = GPIO_Pin_0;
	ADC_Config.GPIO_Pin = pin;
	ADC_Config.GPIO_Mode = GPIO_Mode_AN;
	ADC_Config.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(pin_header, &ADC_Config);


	//ADC configuration
	ADC_CommonInitTypeDef ADC_CommonInitStructure;
	ADC_InitTypeDef ADC_InitStructure;

	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div2;
	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;

	ADC_CommonInit(&ADC_CommonInitStructure);


	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfConversion = 1;

	ADC_StructInit(&ADC_InitStructure);

	ADC_Init(ADCx, &ADC_InitStructure);

			//ADC_RegularChannelConfig(ADC1, ADC_Channel_10, 1, ADC_SampleTime_480Cycles);
	ADC_RegularChannelConfig(ADCx, adc_channel, 1, ADC_SampleTime_480Cycles);
	ADC_EOCOnEachRegularChannelCmd(ADCx, ENABLE);

	//power on ADC
	ADC_Cmd(ADCx, ENABLE);
}

/**
 * @brief Makes one ADC conversion and returns value
 * @param adc_type: ID of ADC to use
 * @retval uint16_t
 */
uint16_t ADCH_getValue(ADC_TypeDef* adc_type, uint8_t channel){

	//Change channel
	ADC_RegularChannelConfig(adc_type, channel, 1, ADC_SampleTime_480Cycles);


	//Make ADC conversion
	ADC_SoftwareStartConv(adc_type);
	while(ADC_GetFlagStatus(adc_type, ADC_FLAG_EOC) == RESET);

	//return value
	return ADC_GetConversionValue(adc_type);
}

uint16_t ADCH_getValueX(ADC_TypeDef* adc_type, uint8_t channel, uint8_t sample_time){

	uint32_t value = 0;
	uint16_t cycles = 1;

	#if FILTER_ON
	cycles = NR_OF_FILTER_STEPS;
	#endif

	//Change channel
	ADC_RegularChannelConfig(adc_type, channel, 1, sample_time);

	int i = 0;
	for(i=0;i<cycles;i++){

		//Make ADC conversion
		ADC_SoftwareStartConv(adc_type);
		while(ADC_GetFlagStatus(adc_type, ADC_FLAG_EOC) == RESET);

		//return value
		value += ADC_GetConversionValue(adc_type);
	}

	value /= cycles;

	return (uint16_t)value;
}




