#ifndef __DEFINES_H__
#define __DEFINES_H__


//settings during testing

//slave_ID: 1->left macceppa		2->back			3->right macceppa
#define SLAVE_ID 2

#define IMU1_ON 0
#define IMU2_ON 0
#define IMU3_ON 0

#define SOFT_POT1_ON 0		//knee ankle
#define SOFT_POT2_ON 0		//lever arm
#define SOFT_POT3_ON 0 		//roll

#define HIP_POT1_ON 1		//left
#define HIP_POT2_ON 0		//right

#define MCU1_ON 0			//left or lever arm
#define MCU2_ON 0			//right or roll

#define POS_CONTROLL_ON 1
#define USB_COMM_ON 1

#define FILTER_ON 1
#define NR_OF_FILTER_STEPS 50




#define MCU_PWM_RESOLUTION (1 << 13)

#define MASTER_COMM_FREQ 20

#define POS_CONTROLL_FREQ 100
#define POS_CONTROLL_VAL_COUNT (int)( POS_CONTROLL_FREQ / MASTER_COMM_FREQ )


#endif
