#ifndef __IMU_HELPER_H__
#define __IMU_HELPER_H__

#include "tm_stm32f4_i2c.h"
#include "data_interface.h"

typedef struct {
   int ADDRESS_A; //Address of the Accelorometer and Magnetometer and temperature sensor
   int ADDRESS_G; //Address of the gyroscope

   //IMU ID
   uint8_t ID;

   //Values of the accelorometer
   int16_t ax;
   int16_t ay;
   int16_t az;

   //Values of the gyroscope
   int16_t gx;
   int16_t gy;
   int16_t gz;

   //Values of the magnetometer
   float mx;
   float my;
   float mz;

   //Resolution factor, depending on the sensor range set in the init
   float gRes;
   float aRes;
   float mRes;

   I2C_TypeDef* I2C_type; //Selection of one of the three i2c modules
   int speed; //speed of the clock 100khz recommended
   TM_I2C_PinsPack_t pins; //pin set of scl sda

   //Temperature of the sensor
   float temp;
}IMU_struct;




void init_imu(IMU_struct s);

void update_gyro_acc(IMU_struct* s);

void initMag(IMU_struct s);
void readMag(IMU_struct* s);

void readTemperature(IMU_struct* s);

void initGyro(IMU_struct s);
void readGyro(IMU_struct* s);

void initAc(IMU_struct s);
void readAc(IMU_struct* s);


void fill_imu_data(Slave_Data *slave_data, IMU_struct *s);





#endif
